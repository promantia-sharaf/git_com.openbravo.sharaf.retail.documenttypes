/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.documenttypes;

import javax.enterprise.context.ApplicationScoped;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.retail.sessions.WebPOSCloseStoreHook;

@ApplicationScoped
public class CloseStoreSessionHook extends WebPOSCloseStoreHook {

  @Override
  public void exec(Organization store) throws Exception {

    final OBCriteria<OBPOSApplications> terminalCriteria = OBDal.getInstance().createCriteria(
        OBPOSApplications.class);
    terminalCriteria.add(Restrictions.eq(OBPOSApplications.PROPERTY_ORGANIZATION, store));
    for (final OBPOSApplications terminal : terminalCriteria.list()) {
      terminal.setReturnslastassignednum(0L);
      terminal.setQuotationslastassignednum(0L);
      terminal.setLastassignednum(0L);
      OBDal.getInstance().save(terminal);
    }
  }

}
