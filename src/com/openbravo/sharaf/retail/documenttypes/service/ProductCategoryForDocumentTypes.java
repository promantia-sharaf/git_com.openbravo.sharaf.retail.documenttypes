/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.documenttypes.service;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLPropertyList;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.mobile.core.model.ModelExtensionUtils;
import org.openbravo.mobile.core.process.ProcessHQLQuery;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.retail.posterminal.POSUtils;

public class ProductCategoryForDocumentTypes extends ProcessHQLQuery {
  public static final String Product_Category_PROPERTY_EXTENSION = "Product_Category_PROPERTY_EXTENSION";
  private static final Logger log = Logger.getLogger(ProductCategoryForDocumentTypes.class);

  // ModelExtension instance that will be injected with all classes that have
  // the SERVEOPTION_PROPERTY_EXTENSION Qualifier annotation
  @Inject
  @Any
  @Qualifier(Product_Category_PROPERTY_EXTENSION)
  private Instance<ModelExtension> extensions;

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    // TODO Auto-generated method stub
    String posId = jsonsent.getString("pos");
    OBPOSApplications terminal = POSUtils.getTerminalById(posId);
    if (terminal == null) {
      throw new JSONException("OBPOS_TerminalNotFound");
    }

    List<String> hqlQueries = new ArrayList<String>();
    HQLPropertyList productcategoryprops = ModelExtensionUtils.getPropertyExtensions(extensions);
    String hql = "select " + productcategoryprops.getHqlSelect()
        + " from custsdt_product_cat productcategory";
    hql += " where productcategory.custsdtDocumenttype.id in (select dt.custsdtDocumenttype.id from custsdt_postype_doctype as dt where dt.active='Y' and dt.pOSTerminalType = '"
        + terminal.getObposTerminaltype().getId()
        + "' )"
        + " and productcategory.active='Y' order by productcategory.updated desc";
    hqlQueries.add(hql);
    return hqlQueries;
  }

}