package com.openbravo.sharaf.retail.documenttypes.service;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;

@Qualifier(ProductForDocumentTypes.Product_PROPERTY_EXTENSION)
public class ProductForDocumentTypesProperties extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    List<HQLProperty> props = new ArrayList<HQLProperty>();
    props.add(new HQLProperty("product.id", "id"));
    props.add(new HQLProperty("product.custsdtDocumenttype.id", "custsdtDocumenttype"));
    props.add(new HQLProperty("product.product.id", "product"));
    props.add(new HQLProperty("product.active", "active"));

    return props;
  }

}