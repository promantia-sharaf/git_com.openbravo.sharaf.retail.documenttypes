package com.openbravo.sharaf.retail.documenttypes.service;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;

@Qualifier(AllowedDeliveryTypes.ALLOWED_DELIVERY_PROPERTY_EXTENSION)
public class AllowedDeliveryTypesProperties extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    List<HQLProperty> props = new ArrayList<HQLProperty>();
    props.add(new HQLProperty("alloweddeltypes.id", "id"));
    props.add(new HQLProperty("alloweddeltypes.custdelDeliverycondition",
        "custdelDeliverycondition"));
    props.add(new HQLProperty("alloweddeltypes.custsdtDocumenttype.id", "custsdtDocumenttype"));
    props.add(new HQLProperty("alloweddeltypes.priority", "priority"));
    props.add(new HQLProperty("alloweddeltypes.active", "active"));

    return props;
  }

}