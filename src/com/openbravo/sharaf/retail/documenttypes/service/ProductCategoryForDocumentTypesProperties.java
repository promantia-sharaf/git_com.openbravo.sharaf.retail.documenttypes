package com.openbravo.sharaf.retail.documenttypes.service;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;

@Qualifier(ProductCategoryForDocumentTypes.Product_Category_PROPERTY_EXTENSION)
public class ProductCategoryForDocumentTypesProperties extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    List<HQLProperty> props = new ArrayList<HQLProperty>();
    props.add(new HQLProperty("productcategory.id", "id"));
    props.add(new HQLProperty("productcategory.custsdtDocumenttype.id", "custsdtDocumenttype"));
    props.add(new HQLProperty("productcategory.productCategory.id", "productCategory"));
    props.add(new HQLProperty("productcategory.active", "active"));

    return props;
  }

}