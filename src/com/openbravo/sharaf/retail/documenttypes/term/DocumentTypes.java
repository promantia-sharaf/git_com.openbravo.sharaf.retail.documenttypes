/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.documenttypes.term;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.retail.posterminal.POSUtils;
import org.openbravo.retail.posterminal.term.QueryTerminalProperty;

import com.openbravo.sharaf.retail.documenttypes.CustsdtDocumenttype;

public class DocumentTypes extends QueryTerminalProperty {

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    String posId = jsonsent.getString("pos");
    OBPOSApplications terminal = POSUtils.getTerminalById(posId);
    if (terminal == null) {
      CustsdtDocumenttype s = new CustsdtDocumenttype();
      throw new JSONException("OBPOS_TerminalNotFound");
    }

    // To return document types which has preferred return document type.
    List<String> list1 = Arrays
        .asList(new String[] { "select dt.custsdtDocumenttype.id as id, dt.custsdtDocumenttype.searchKey as searchKey, "
            + "dt.custsdtDocumenttype.prefix as prefix, dt.custsdtDocumenttype.name as name, "
            + "dt.custsdtDocumenttype.ticketSequence as ticketSequence, dt.defaultTicket as defaultTicket, "
            + "dt.defaultQuotation as defaultQuotation, dt.defaultReturn as defaultReturn, "
            + "dt.custsdtDocumenttype.quotationSequence as quotationSequence, dt.custsdtDocumenttype.returnSequence as returnSequence, dt.custsdtDocumenttype.custshaAllowPriceModify as custshaAllowPriceModify, dt.custsdtDocumenttype.arabicName as arabicName, "
            + "dt.custsdtDocumenttype.preferredReturnDocumentType.name as preferredReturnDocumentType, " // to
                                                                                                         // return
                                                                                                         // preferred
                                                                                                         // return
                                                                                                         // doc
                                                                                                         // type
            + "dt.custsdtDocumenttype.shfptDoctypeHeader as shfptDoctypeHeader, dt.custsdtDocumenttype.shfptDoctypeFooter as shfptDoctypeFooter, "
            + "dt.custsdtDocumenttype.shfptEndCustomer as shfptEndCustomer, "
            + "dt.custsdtDocumenttype.prodCatSelection as prodCatSelection, "
            + "dt.custsdtDocumenttype.productSelection as productSelection "
            + "from custsdt_postype_doctype dt " //
            + "where $naturalOrgCriteria and $activeCriteria and dt.custsdtDocumenttype.active = 'Y' " //
            + "and dt.pOSTerminalType.id = '" + terminal.getObposTerminaltype().getId() + "'" //
            + "order by dt.custsdtDocumenttype.name asc" });

    // to return document types where Preferred Return Document Type is null.
    List<String> list2 = Arrays
        .asList(new String[] { "select dt.custsdtDocumenttype.id as id, dt.custsdtDocumenttype.searchKey as searchKey, "
            + "dt.custsdtDocumenttype.prefix as prefix, dt.custsdtDocumenttype.name as name, "
            + "dt.custsdtDocumenttype.ticketSequence as ticketSequence, dt.defaultTicket as defaultTicket, "
            + "dt.defaultQuotation as defaultQuotation, dt.defaultReturn as defaultReturn, "
            + "dt.custsdtDocumenttype.quotationSequence as quotationSequence, dt.custsdtDocumenttype.returnSequence as returnSequence, dt.custsdtDocumenttype.custshaAllowPriceModify as custshaAllowPriceModify, dt.custsdtDocumenttype.arabicName as arabicName, "
            + "dt.custsdtDocumenttype.shfptDoctypeHeader as shfptDoctypeHeader, dt.custsdtDocumenttype.shfptDoctypeFooter as shfptDoctypeFooter, "
            + "dt.custsdtDocumenttype.shfptEndCustomer as shfptEndCustomer, "
            + "dt.custsdtDocumenttype.prodCatSelection as prodCatSelection, "
            + "dt.custsdtDocumenttype.productSelection as productSelection "
            + "from custsdt_postype_doctype dt " //
            + "where $naturalOrgCriteria and $activeCriteria and dt.custsdtDocumenttype.active = 'Y' " //
            + "and dt.custsdtDocumenttype.preferredReturnDocumentType is null "
            + "and dt.pOSTerminalType.id = '" + terminal.getObposTerminaltype().getId() + "'" //
            + "order by dt.custsdtDocumenttype.name asc" });

    List<String> list3 = new ArrayList<String>();
    list3.addAll(list1);
    list3.addAll(list2);
    return list3;
  }

  @Override
  protected boolean bypassPreferenceCheck() {
    return true;
  }

  @Override
  public String getProperty() {
    return "sharafDocumentType";
  }

  @Override
  public boolean returnList() {
    return false;
  }
}
