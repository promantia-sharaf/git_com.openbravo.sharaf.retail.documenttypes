/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.documenttypes.term;

import java.util.Arrays;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.model.ad.access.User;
import org.openbravo.retail.posterminal.term.QueryTerminalProperty;

public class SellerInfo extends QueryTerminalProperty {

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    User user = OBContext.getOBContext().getUser();
    if (user == null) {
      throw new JSONException("OBPOS_TerminalNotFound");
    }

    return Arrays
        .asList(new String[] { "select username as sellerCode, businessPartner.name as sellerName from ADUser " //
            + "where id = '" + user.getId() + "'" });
  }

  @Override
  protected boolean bypassPreferenceCheck() {
    return true;
  }

  @Override
  public String getProperty() {
    return "sharafSellerInfo";
  }

  @Override
  public boolean returnList() {
    return false;
  }
}
