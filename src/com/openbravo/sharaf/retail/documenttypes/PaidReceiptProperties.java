/*
 ************************************************************************************
 * Copyright (C) 2013 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.documenttypes;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.PaidReceipts;

/**
 * Defines hql properties for the PaidReceips Order header
 * 
 * @author EBE
 * 
 */

@Qualifier(PaidReceipts.paidReceiptsPropertyExtension)
public class PaidReceiptProperties extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    ArrayList<HQLProperty> list = new ArrayList<HQLProperty>() {
      private static final long serialVersionUID = 1L;
      {
        add(new HQLProperty("ord.custsdtDocumenttype.id", "custsdtDocumenttype"));
        add(new HQLProperty("ord.custsdtDocumenttype.searchKey", "custsdtDocumenttypeSearchKey"));
      }
    };

    return list;
  }
}
