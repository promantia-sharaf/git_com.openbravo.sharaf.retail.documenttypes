/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.documenttypes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.BaseComponentProvider.ComponentResource.ComponentResourceType;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.retail.posterminal.POSUtils;

/**
 * @author ebe
 * 
 */
@ApplicationScoped
@ComponentProvider.Qualifier(CUSTSDTComponentProvider.QUALIFIER)
public class CUSTSDTComponentProvider extends BaseComponentProvider {

  public static final String QUALIFIER = "CUSTSDT_Main";
  public static final String MODULE_JAVA_PACKAGE = "com.openbravo.sharaf.retail.documenttypes";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {

    final GlobalResourcesHelper grhelper = new GlobalResourcesHelper();

    grhelper.add("components/menu.js");
    grhelper.add("components/modelDocumentTypeFilter.js");
    grhelper.add("components/modalDocumentTypeSelector.js");
    grhelper.add("components/orderViewDivText.js");
    grhelper.add("components/updateCreditSalesActionPaymentExtension.js");
    grhelper.add("hooks/OBPOS_NewReceipt.js");
    grhelper.add("hooks/OBPOS_PreAddProductToOrder.js");
    grhelper.add("hooks/OBPOS_PrePaymentHook.js");
    grhelper.add("hooks/OBPOS_PreProcessCancelLayaway.js");
    grhelper.add("hooks/OBPOS_PostAddProductToOrder.js");
    grhelper.add("hooks/windowReadyListerner.js");
    grhelper.add("hooks/OBPOS_ChangeBusinessDateHook.js");
    grhelper.add("hooks/OBRETUR_ReturnFromOrig.js");
    grhelper.add("hooks/OBRETUR_ReturnReceiptApply.js");
    grhelper.add("model/alloweddeliverytypes.js");
    grhelper.add("model/productcategoryfordocumenttypes.js");
    grhelper.add("model/productfordocumenttypes.js");
    grhelper.add("utils.js");

    return grhelper.getGlobalResources();
  }

  private class GlobalResourcesHelper {
    private final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();
    private final String prefix = "web/" + MODULE_JAVA_PACKAGE + "/js/";

    public void add(String file) {
      globalResources.add(createComponentResource(ComponentResourceType.Static, prefix + file,
          POSUtils.APP_NAME));
    }

    public List<ComponentResource> getGlobalResources() {
      return globalResources;
    }
  }
}
