/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.documenttypes.hooks;

import javax.enterprise.context.ApplicationScoped;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.retail.posterminal.OrderLoaderPreProcessHook;

@ApplicationScoped
public class SharafDocTypesPreOrderLoaderHook implements OrderLoaderPreProcessHook {

  @Override
  public void exec(JSONObject jsonorder) throws Exception {
    final boolean isQuotation = jsonorder.has("isQuotation") && jsonorder.getBoolean("isQuotation");
    String docTypePrefix = jsonorder.getString("documentNo").substring(0, 2);
    String documentNoPrefix = null;
    String newDocumentNoPrefix = null;
    if (isQuotation) {
      if (jsonorder.has("quotationnoPrefix")) {
        documentNoPrefix = jsonorder.getString("quotationnoPrefix");
        newDocumentNoPrefix = docTypePrefix.concat(documentNoPrefix.substring(2));
        jsonorder.put("quotationnoPrefix", newDocumentNoPrefix);
      }
    } else if (jsonorder.optLong("returnnoSuffix", -1L) > -1L) {
      if (jsonorder.has("returnnoPrefix")) {
        documentNoPrefix = jsonorder.getString("returnnoPrefix");
        newDocumentNoPrefix = docTypePrefix.concat(documentNoPrefix.substring(2));
        jsonorder.put("returnnoPrefix", newDocumentNoPrefix);
      }
    } else {
      if (jsonorder.has("documentnoPrefix")) {
        documentNoPrefix = jsonorder.getString("documentnoPrefix");
        newDocumentNoPrefix = docTypePrefix.concat(documentNoPrefix.substring(2));
        jsonorder.put("documentnoPrefix", newDocumentNoPrefix);
      }
    }
  }
}
