/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.documenttypes.hooks;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.CancelAndReplaceOrderHook;
import org.openbravo.model.common.order.Order;

import com.openbravo.sharaf.retail.documenttypes.CustsdtDocumenttype;

public class CancelAndReplaceHook extends CancelAndReplaceOrderHook {

  /**
   * Hook executed during the Cancel and Replace and Cancel Layaway processes. Set 'Advance Refund'
   * Sharaf Document Type to inverse orderr
   */
  @Override
  public void exec(boolean replaceOrder, boolean triggersDisabled, Order oldOrder, Order newOrder,
      Order inverseOrder, JSONObject jsonorder) throws Exception {
    if (jsonorder.has("custsdtDocumenttype")) {
      final CustsdtDocumenttype docType = OBDal.getInstance().get(CustsdtDocumenttype.class,
          jsonorder.get("custsdtDocumenttype"));
      inverseOrder.setCustsdtDocumenttype(docType);
    }
  }
}
