/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.documenttypes;

import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.model.ad.access.User;
import org.openbravo.retail.posterminal.CustomInitialValidation;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.retail.posterminal.TerminalType;

public class InitialValidation extends CustomInitialValidation {

  @Override
  public void validation(OBPOSApplications posTerminal) throws JSONException {

    // Validation: Sharaf Document Types
    TerminalType terminalType = posTerminal.getObposTerminaltype();
    List<CustsdtPostypeDoctype> docTypes = terminalType.getCustsdtPostypeDoctypeList();
    if (docTypes.size() == 0) {
      throw new JSONException("CUSTSDT_errorLoadingDocumentTypes");
    }
    boolean ticketSequence = false, quotationSequence = false, returnSequence = false;
    for (CustsdtPostypeDoctype docType : docTypes) {
      CustsdtDocumenttype dt = docType.getCustsdtDocumenttype();
      if (dt.isTicketSequence()) {
        ticketSequence = true;
      }
      if (dt.isQuotationSequence()) {
        quotationSequence = true;
      }
      if (dt.isReturnSequence()) {
        returnSequence = true;
      }
      if (ticketSequence && quotationSequence && returnSequence) {
        break;
      }
    }

    if (!ticketSequence) {
      throw new JSONException("CUSTSDT_errorLoadingDocumentTypesForTickets");
    }
    if (!quotationSequence) {
      throw new JSONException("CUSTSDT_errorLoadingDocumentTypesForQuotations");
    }
    if (!returnSequence) {
      throw new JSONException("CUSTSDT_errorLoadingDocumentTypesForReturns");
    }

    // Validation: Seller have BP associate
    User user = OBContext.getOBContext().getUser();
    if (user == null) {
      throw new JSONException("OBPOS_TerminalNotFound");
    }
    if (user.getBusinessPartner() == null) {
      throw new JSONException("CUSTSDT_errorLoadingSellerNotHaveBP");
    }

  }

}
