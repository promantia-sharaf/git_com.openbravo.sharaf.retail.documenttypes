/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

enyo.kind({
  name: 'CUSTSDT.UI.DocumentTypeScrollableHeader',
  kind: 'OB.UI.ScrollableTableHeader',
  components: [{
    style: 'padding: 10px;',
    kind: 'OB.UI.FilterSelectorTableHeader',
    name: 'filterSelector',
    filters: OB.Model.CUSTSDT_DocumentTypeFilter.getProperties()
  }]
});

enyo.kind({
  name: 'CUSTSDT.UI.DocumentTypeRenderLine',
  kind: 'OB.UI.ListSelectorLine',
  allowHtml: true,
  components: [{
    name: 'line',
    style: 'line-height: 23px; width: 100%',
    components: [{
      name: 'textInfo',
      style: 'float: left; width: 91%; min-height: 32px; padding-top: 10px;',
      components: [{
        style: 'float: left; display: inline-block;',
        name: 'name'
      }, {
        style: 'float: left; display: inline-block; color: #888888; padding-left:5px;',
        name: 'prefix'
      }, {
        style: 'clear: both;'
      }]
    }]
  }],
  create: function () {
    this.inherited(arguments);
    this.$.name.setContent(this.model.get('name'));
    this.$.prefix.setContent(' ( ' + this.model.get('prefix') + ' )');
    this.render();
  }
});

enyo.kind({
  name: 'CUSTSDT.UI.DocumentTypeList',
  classes: 'row-fluid',
  handlers: {
    onSearchAction: 'searchAction',
    onClearFilterSelector: 'clearAction'
  },
  events: {
    onChangeFilterSelector: ''
  },
  components: [{
    classes: 'span12',
    components: [{
      style: 'border-bottom: 1px solid #cccccc;',
      classes: 'row-fluid',
      components: [{
        classes: 'span12',
        components: [{
          name: 'docTypeTable',
          kind: 'OB.UI.ScrollableTable',
          classes: 'bp-scroller',
          scrollAreaMaxHeight: '400px',
          renderHeader: 'CUSTSDT.UI.DocumentTypeScrollableHeader',
          renderLine: 'CUSTSDT.UI.DocumentTypeRenderLine',
          renderEmpty: 'OB.UI.RenderEmpty'
        }, {
          name: 'renderLoading',
          style: 'border-bottom: 1px solid #cccccc; padding: 20px; text-align: center; font-weight: bold; font-size: 30px; color: #cccccc',
          showing: false,
          initComponents: function () {
            this.setContent(OB.I18N.getLabel('OBPOS_LblLoading'));
          }
        }]
      }]
    }]
  }],
  clearAction: function (inSender, inEvent) {
    this.itemsList.reset();
    return true;
  },
  searchAction: function (inSender, inEvent) {
    var documentTypes = OB.MobileApp.model.get('sharafDocumentType'),
        orderType = OB.MobileApp.model.receipt.get('orderType'),
        isQuotation = OB.MobileApp.model.receipt.get('isQuotation'),
        negativeLines;
    negativeLines = _.filter(OB.MobileApp.model.receipt.get('lines').models, function (line) {
      return line.get('qty') < 0;
    }).length;
    // Filter available document types based on the orderType of the current order
    // Tickets and Quotations
    if (orderType === 0) {
      if (isQuotation) {
        documentTypes = _.filter(documentTypes, function (item) {
          return item.quotationSequence;
        });
      } else if (negativeLines > 0) {
        documentTypes = _.filter(documentTypes, function (item) {
          return item.returnSequence && item.searchKey !== 'AR';
        });
      } else {
        documentTypes = _.filter(documentTypes, function (item) {
          return item.ticketSequence && item.searchKey !== 'AD' && item.searchKey !== 'AR';
        });
      }
      // Returns
    } else if (orderType === 1) {
      documentTypes = _.filter(documentTypes, function (item) {
        return item.returnSequence && item.searchKey !== 'AR';
      });
      // Layaways
    } else if (orderType === 2) {
      documentTypes = _.filter(documentTypes, function (item) {
        return item.searchKey === 'AD';
      });
      // The rest (Cancel Layaway)
    } else {
      documentTypes = _.filter(documentTypes, function (item) {
        return false;
      });
    }
    if (inEvent && inEvent.filters) {
      var flt = inEvent.filters[0].value.toLowerCase();
      documentTypes = _.filter(documentTypes, function (item) {
        return item.name.toLowerCase().indexOf(flt) >= 0;
      });
    }
    this.itemsList.reset(documentTypes);
    return true;
  },
  itemsList: null,
  init: function (model) {
    var me = this;
    this.itemsList = new Backbone.Collection();
    this.$.docTypeTable.setCollection(this.itemsList);
    this.itemsList.on('click', function (model) {
      OB.CUSTSDT.Utils.changeDocumentType(OB.MobileApp.model.receipt, model);
      if (OB.MobileApp.model.get('lastPaneShown') === 'payment') {
        OB.MobileApp.model.receipt.trigger('scan');
      }
    }, this);
  }
});

/* Modal definition */
enyo.kind({
  kind: 'OB.UI.ModalSelector',
  name: 'CUSTSDT.UI.ModalDocumentTypeSelector',
  topPosition: '45px',
  style: 'width: 725px',
  i18nHeader: 'CUSTSDT_LblDocumentTypeSelectorCaption',
  body: {
    kind: 'CUSTSDT.UI.DocumentTypeList'
  },
  executeOnShow: function () {
    if (!this.initialized) {
      this.inherited(arguments);
      this.getFilterSelectorTableHeader().searchAction();
    }
  },
  getFilterSelectorTableHeader: function () {
    return this.$.body.$.documentTypeList.$.docTypeTable.$.theader.$.documentTypeScrollableHeader.$.filterSelector;
  },
  init: function (model) {
    this.inherited(arguments);
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTSDT.UI.ModalDocumentTypeSelector',
  name: 'CUSTSDT_ModalDocumentTypeSelector'
});