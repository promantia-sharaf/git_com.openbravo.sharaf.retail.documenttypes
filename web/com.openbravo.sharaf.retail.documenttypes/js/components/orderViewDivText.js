/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () {

  OB.UI.OrderViewDivText.extend({
    changeOrderTypeDocumentNo: function (model) {
      this.hide();
    },

    changeIsQuotation: function (model) {
      this.hide();
    },

    changeHasbeenpaid: function (model) {
      this.hide();
    },

    changeIsPaidPaidOnCreditIsQuotationDocumentNoPaidPartiallyOnCredit: function (model) {
      this.hide();
    },

    changeIsLayaway: function (model) {
      this.hide();
    },

    changeReplacedorder: function (model) {
      this.hide();
    }
  });

  enyo.kind({
    name: 'OB.POS.ORDERFOOTER.ReceiptDocumentTypes',
    style: 'float: right; text-align: right; font-weight:bold; font-size: 30px; line-height: 33px; color: rgb(248, 148, 29);',
    init: function (model) {
      model.get('order').on('change:custsdtDocumenttype', function (model) {
        var sharafDocumentType = _.find(OB.MobileApp.model.get('sharafDocumentType'), function (item) {
          return item.id === model.get('custsdtDocumenttype');
        });
        this.setContent(sharafDocumentType ? sharafDocumentType.name : '');
      }, this);

      model.get('order').on('change:documentType', function (model) {
        if (model.get('documentType') === OB.MobileApp.model.get('terminal').terminalType.documentTypeForReturns && (!model.get('custsdtDocumenttypeSearchKey') || (model.get('custsdtDocumenttypeSearchKey') && !OB.CUSTSDT.Utils.getDocumentTypeBySearchKey(model.get('custsdtDocumenttypeSearchKey')).returnSequence))) {
          OB.CUSTSDT.Utils.updateOrderDocumentType(model, 'returnSequence', 'defaultReturn');
        }
      }, this);

      model.get('order').on('change:documentNo', function (model) {
        var documentNo = model.get('documentNo');
        if (documentNo.indexOf(OB.MobileApp.model.get('terminal').returnDocNoPrefix) === 0) {
          OB.CUSTSDT.Utils.updateOrderDocumentType(model, 'returnSequence', 'defaultReturn');
        } else if (documentNo.indexOf(OB.MobileApp.model.get('terminal').docNoPrefix) === 0) {
          OB.CUSTSDT.Utils.updateOrderDocumentType(model, 'ticketSequence', 'defaultTicket');
        }
      }, this);

      model.get('order').on('change:orderType', function (model) {
        var orderType = model.get('orderType'),
            documentNo = model.get('documentNo');
        if (orderType === 2) {
          documentNo = 'AD' + documentNo.substring(2);
          model.set('documentNo', documentNo, {
            silent: true
          });
          OB.CUSTSDT.Utils.updateOrderDocType(model, OB.CUSTSDT.Utils.getDocumentTypeBySearchKey('AD'));
        } else if (orderType === 0) {
          documentNo = OB.CUSTSDT.Utils.getDocumentType('ticketSequence', 'defaultTicket').prefix + documentNo.substring(2);
          model.set('documentNo', documentNo, {
            silent: true
          });
          OB.CUSTSDT.Utils.updateOrderDocumentType(model, 'ticketSequence', 'defaultTicket');
        } else if (orderType === 3) {
          var docType = OB.CUSTSDT.Utils.getDocumentTypeBySearchKey('AR');
          if (docType) {
            OB.CUSTSDT.Utils.changeDocumentType(model, new Backbone.Model(docType));
          }
        }
      }, this);
    }
  });

}());