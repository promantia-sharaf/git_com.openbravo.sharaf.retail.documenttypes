/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone */

// Hide "Use Credit" button if the Document Type is not "Debtor Sale" or "Debtor Refund"
OB.OBPOSPointOfSale.UI.Payment.extend({
  updateCreditSalesAction: function () {
    // The terminal allows to pay on credit
    var visible = OB.MobileApp.model.get('terminal').allowpayoncredit;
    // And is a loaded layaway or a regular order (no new layaway and no voided layaway)
    // this.receipt.get('orderType') === 2 --> New layaway 
    // this.receipt.get('orderType') === 3 --> Voided layaway 
    // this.receipt.get('isLayaway') --> Loaded layaway    
    visible = visible && ((this.receipt.get('isLayaway') || this.receipt.get('orderType') !== 2) && this.receipt.get('orderType') !== 3);
    // And receipt has not been paid
    visible = visible && !this.receipt.getPaymentStatus().done;
    // And Business Partner exists and is elegible to sell on credit.
    visible = visible && this.receipt.get('bp') && (this.receipt.get('bp').get('creditLimit') > 0 || this.receipt.get('bp').get('creditUsed') < 0);

    var returnDocTypesArr = [];
    for (var i = 0; i < OB.MobileApp.model.attributes.sharafDocumentType.length; i++) {
        var docs = OB.MobileApp.model.attributes.sharafDocumentType[i];
        if ((!docs.ticketSequence) && docs.returnSequence) {
        	returnDocTypesArr.push(docs.searchKey);
        }
    }
    if (returnDocTypesArr.includes(OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey'))){
    	visible = true;
    }
    
    if (visible) {
      this.$.creditsalesaction.show();
    } else {
      this.$.creditsalesaction.hide();
    }
    if (this.receipt.get('custsdtDocumenttypeSearchKey') === 'SI') {
      this.$.creditsalesaction.hide();
    }
  }
});