/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global Backbone, _ */

(function () {

  OB.Model.CUSTSDT_DocumentTypeFilter = OB.Data.ExtensibleModel.extend({});

  OB.Model.CUSTSDT_DocumentTypeFilter.addProperties([{
    name: 'id',
    column: 'id',
    filter: false,
    type: 'TEXT',
    operator: OB.Dal.EQ
  }, {
    name: 'name',
    column: 'name',
    filter: true,
    type: 'TEXT',
    operator: OB.Dal.CONTAINS,
    caption: 'CUSTSDT_LblName',
    isFixed: true
  }]);

}());