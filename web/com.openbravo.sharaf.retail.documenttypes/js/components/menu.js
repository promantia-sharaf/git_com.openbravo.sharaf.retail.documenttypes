/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  enyo.kind({
    name: 'CUSTSDT.UI.MenuDocumentTypeSelector',
    kind: 'OB.UI.MenuAction',
    i18nLabel: 'CUSTSDT_LblDocumentTypeSelector',
    permission: 'CUSTSDT_DocumentType.change',
    events: {
      onShowPopup: ''
    },
    tap: function () {
      this.inherited(arguments); // auto close the menu
      var creditNotePayment = _.find(OB.MobileApp.model.receipt.attributes.payments.models, function (p) {
        return p.get('kind') === 'GCNV_payment.creditnote';
      });
      if (!OB.UTIL.isNullOrUndefined(creditNotePayment) && !OB.UTIL.isNullOrUndefined(creditNotePayment.get('creditnoteId'))) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTSDT_restrictChangeDocumentTypeHeader'), OB.I18N.getLabel('CUSTSDT_restrictChangeDocumentTypeBody'), [{
          label: OB.I18N.getLabel('OBMOBC_LblOk')
        }]);
      } else if (OB.MobileApp.model.receipt.get('isEditable') === false) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBPOS_modalNoEditableHeader'), OB.I18N.getLabel('OBPOS_modalNoEditableBody'), [{
          label: OB.I18N.getLabel('OBMOBC_LblOk')
        }]);
      } else {
    	  if (OB.MobileApp.model.receipt.attributes.lines.models.length > 0){
    		  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTSDT_restrictChangeDocumentType'), [{
    	          label: OB.I18N.getLabel('OBMOBC_LblOk')
    	      }]);
    	  } else {
    		  this.doShowPopup({
    			  popup: 'CUSTSDT_ModalDocumentTypeSelector'
    		  });
    	  }
      }
    },
    init: function (model) {
      this.model = model;
      if (OB.MobileApp.model.hasPermission(this.permission, true)) {
        this.show();
      } else {
        this.hide();
      }
    }
  });

  // Register menu items
  OB.OBPOSPointOfSale.UI.LeftToolbarImpl.prototype.menuEntries.push({
    kind: 'CUSTSDT.UI.MenuDocumentTypeSelector'
  });

}());