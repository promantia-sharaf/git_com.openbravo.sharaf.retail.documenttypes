/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global, OB, moment, _ */

(function () {

  OB.CUSTSDT = OB.CUSTSDT || {};

  OB.CUSTSDT.Utils = {

    getDocumentType: function (sequenceField, defaultField) {
      var docType = null,
          documentTypes = _.filter(OB.MobileApp.model.get('sharafDocumentType'), function (item) {
          return item[sequenceField];
        });
      _.each(documentTypes, function (dt) {
        if (dt[defaultField]) {
          docType = dt;
        }
      });
      if (docType === null && documentTypes.length > 0) {
        docType = documentTypes[0];
      }
      return docType;
    },

    getDocumentTypeBySearchKey: function (searchKey) {
      var documentType = _.find(OB.MobileApp.model.get('sharafDocumentType'), function (item) {
        return item.searchKey === searchKey;
      });
      return documentType;
    },

    getDocumentPrefix: function (docType, prefix, businessdate) {
      if (!docType) {
        return prefix;
      }
      return docType.prefix + prefix + businessdate;
    },

    updateOrderDocumentType: function (order, sequenceField, defaultField) {
      var docType = this.getDocumentType(sequenceField, defaultField);
      this.updateOrderDocType(order, docType);
    },

    updateOrderDocType: function (order, docType) {
      if (docType && order.get('custsdtDocumenttype') !== docType.id) {
        order.set('custsdtDocumenttype', docType.id);
        order.set('custsdtDocumenttypeSearchKey', docType.searchKey);
      }
    },

    eventChangeDocumentNo: function (changedModel) {
      changedModel.off('change:documentNo', this.eventChangeDocumentNo);
      if (changedModel.get('isQuotation')) {
        OB.CUSTSDT.Utils.updateOrderDocumentType(changedModel, 'quotationSequence', 'defaultQuotation');
      }
    },

    setDefaultDocumentPrefix: function (businessdate) {
      // Update document number prefix
      var ticketSequence = this.getDocumentType('ticketSequence', 'defaultTicket'),
          quotationSequence = this.getDocumentType('quotationSequence', 'defaultQuotation'),
          returnSequence = this.getDocumentType('returnSequence', 'defaultReturn'),
          docNoPrefix = OB.MobileApp.model.get('terminal').originalDocNoPrefix,
          terminal = OB.MobileApp.model.get('terminal');

      terminal.docNoPrefix = this.getDocumentPrefix(ticketSequence, docNoPrefix, businessdate);
      terminal.quotationDocNoPrefix = this.getDocumentPrefix(quotationSequence, docNoPrefix, businessdate);
      terminal.returnDocNoPrefix = this.getDocumentPrefix(returnSequence, docNoPrefix, businessdate);
    },

    changeDocumentType: function (receipt, docType) {
      var documentNo = receipt.get('documentNo');
      documentNo = docType.get('prefix') + documentNo.substring(2);
      receipt.set('documentNo', documentNo, {
        silent: true
      });
      receipt.set('custsdtDocumenttypeSearchKey', docType.get('searchKey'));
      receipt.set('custsdtDocumenttype', docType.get('id'));
    },

    fetchProdAndProdCatForDocumentType: function (documentSK, documentTypeId, prodCatArr, prodArr, prod, callback) {

      var query = "SELECT pc.* from productcategoryfordocumenttypes pc where pc.custsdt_documenttype_id ='" + documentTypeId + "'and pc.active = 'true'";
      OB.Dal.queryUsingCache(OB.Model.ProductCategoryForDocumentTypes, query, [], function (pc) {
        if (pc.length === 0) {
          var query = "SELECT prd.* from productfordocumenttypes prd where prd.custsdt_documenttype_id ='" + documentTypeId + "'and prd.active = 'true'";
          OB.Dal.queryUsingCache(OB.Model.ProductForDocumentTypes, query, [], function (prd) {
            if (prd.length === 0) {
              for (var i = 0; i < OB.MobileApp.model.attributes.sharafDocumentType.length; i++) {
                var docs = OB.MobileApp.model.attributes.sharafDocumentType[i];
                if (docs.searchKey === documentSK) {
                  if (docs.prodCatSelection && docs.productSelection) { // all excluding defined product categories and products
                    callback();
                  } else if (!docs.prodCatSelection && docs.productSelection) { // only those defined product categories and products
                    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), prod.get('searchkey') + 'not allowed');
                    return;
                  }
                  break;
                }
              }
            } else {
              for (var i = 0; i < prd.models.length; i++) {
                prodArr.push(prd.models[i].attributes.product);
              }
              callback();
            }
          }, function () {
            callback();
          });
        } else {
          for (var i = 0; i < pc.models.length; i++) {
            prodCatArr.push(pc.models[i].attributes.productCategory);
          }
          var query = "SELECT prd.* from productfordocumenttypes prd where prd.custsdt_documenttype_id ='" + documentTypeId + "'and prd.active = 'true'";
          OB.Dal.queryUsingCache(OB.Model.ProductForDocumentTypes, query, [], function (prd) {
            if (prd.length > 0) {
              for (var i = 0; i < prd.models.length; i++) {
                prodArr.push(prd.models[i].attributes.product);
              }
              callback();
            } else {
              callback();
            }
          }, function () {
            callback();
          });
        }
      }, function () {
        callback();
      });
    },

    checkProdAndProdCatForDocumentType: function (documentSK, prodCatArr, prodArr, prod, isError, callback) {
      for (var i = 0; i < OB.MobileApp.model.attributes.sharafDocumentType.length; i++) {
        var docs = OB.MobileApp.model.attributes.sharafDocumentType[i];
        if (docs.searchKey === documentSK) {
          if (docs.prodCatSelection === 'Y') { // all excluding defined product categories
            if (docs.productSelection === 'Y') { // all excluding defined products
              if (prodCatArr.length > 0 && prodArr.length > 0) { // product cat & product both defined
                if ((!prodCatArr.includes(prod.get('productCategory'))) && (!prodArr.includes(prod.get('id')))) {
                  prod.set('isError', false);
                  callback();
                } else {
                  prod.set('isError', true);
                  callback();
                }
                // break;
              } else if (prodCatArr.length > 0 && (!prodArr.length > 0)) { // only product category defined
                if (!prodCatArr.includes(prod.get('productCategory'))) {
                  prod.set('isError', false);
                  callback();
                } else {
                  prod.set('isError', true);
                  callback();
                }
              } else if ((!prodCatArr.length > 0) && (prodArr.length > 0)) { // only product defined
                if (!prodArr.includes(prod.get('id'))) {
                  prod.set('isError', false);
                  callback();
                } else {
                  prod.set('isError', true);
                  callback();
                }
              } else if (!(prodCatArr.length > 0 && prodArr.length > 0)) {
                prod.set('isError', false);
                callback();
              } else {
                prod.set('isError', true);
                callback();
              }
            } else { // only those defined products
              if (prodCatArr.length > 0 && prodArr.length > 0) { // product cat & product both defined
                if (!prodCatArr.includes(prod.get('productCategory')) && prodArr.includes(prod.get('id'))) {
                  prod.set('isError', false);
                  callback();
                } else {
                  prod.set('isError', true);
                  callback();
                }
              } else if (prodCatArr.length > 0 && (!prodArr.length > 0)) { // only product category defined
                if (!prodCatArr.includes(prod.get('productCategory'))) {
                  prod.set('isError', false);
                  callback();
                  break;
                } else {
                  prod.set('isError', true);
                  callback();
                }
              } else if ((!prodCatArr.length > 0) && (prodArr.length > 0)) { // only product defined
                if (prodArr.includes(prod.get('id'))) {
                  prod.set('isError', false);
                  callback();
                } else {
                  prod.set('isError', true);
                  callback();
                }
              } else {
                prod.set('isError', true);
                callback();
              }
            }
          } else { // only those defined product categories
            if (docs.productSelection === 'N') { // only those defined products
              if (prodCatArr.length > 0 && prodArr.length > 0) { // product cat & product both defined
                if (prodCatArr.includes(prod.get('productCategory')) && prodArr.includes(prod.get('id'))) {
                  prod.set('isError', false);
                  callback();
                } else {
                  prod.set('isError', true);
                  callback();
                }
              } else if (prodCatArr.length > 0 && (!prodArr.length > 0)) { // only product category defined
                if (prodCatArr.includes(prod.get('productCategory'))) {
                  prod.set('isError', false);
                  callback();
                } else {
                  prod.set('isError', true);
                  callback();
                }
              } else if ((!prodCatArr.length > 0) && (prodArr.length > 0)) { // only product defined
                if (prodArr.includes(prod.get('id'))) {
                  prod.set('isError', false);
                  callback();
                } else {
                  prod.set('isError', true);
                  callback();
                }
              } else {
                prod.set('isError', true);
                callback();
              }
            } else { // all excluding defined products
              if (prodCatArr.length > 0 && prodArr.length > 0) { // product cat & product both defined
                if (prodCatArr.includes(prod.get('productCategory')) && (!prodArr.includes(prod.get('id')))) {
                  prod.set('isError', false);
                  callback();
                } else {
                  prod.set('isError', true);
                  callback();
                }
              } else if (prodCatArr.length > 0 && (!prodArr.length > 0)) { // only product category defined
                if (prodCatArr.includes(prod.get('productCategory'))) {
                  prod.set('isError', false);
                  callback();
                } else {
                  prod.set('isError', true);
                  callback();
                }
              } else if ((!prodCatArr.length > 0) && (prodArr.length > 0)) { // only product defined
                if (prodArr.includes(prod.get('id'))) {
                  prod.set('isError', false);
                  callback();
                } else {
                  prod.set('isError', true);
                  callback();
                }
              } else {
                prod.set('isError', true);
                callback();
              }
            }
          }
        }
      }
    }
  };

}());