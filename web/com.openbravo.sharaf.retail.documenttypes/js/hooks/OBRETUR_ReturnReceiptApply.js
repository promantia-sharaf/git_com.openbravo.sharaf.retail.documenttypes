/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {
  OB.UTIL.HookManager.registerHook('OBRETUR_ReturnReceiptApply', function (args, callbacks) {

    if (args.selectedLines.length === 0) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    }

    var retDocumentType = OB.COSRD.getSearchKeyFromDocumentTypeName(OB.COSRD.getSearchKeyFromDocumentType(args.order.custsdtDocumenttype).preferredReturnDocumentType).id,
        retDocTypeSK = OB.COSRD.getSearchKeyFromDocumentTypeName(OB.COSRD.getSearchKeyFromDocumentType(args.order.custsdtDocumenttype).preferredReturnDocumentType).searchKey,
        prodArr = [],
        prodCatArr = [],
        errors = [],
        count = 0,
        isError = false;

    _.each(args.selectedLines, function (selectedLines) {
      var query = "SELECT prd.* from m_product prd where prd.description ='" + selectedLines.line.name + "'";
      OB.Dal.queryUsingCache(OB.Model.Product, query, [], function (product) {
        if (product.length === 0) {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        } else {
          var prod = product.models[0];
          OB.CUSTSDT.Utils.fetchProdAndProdCatForDocumentType(retDocTypeSK, retDocumentType, prodCatArr, prodArr, prod, function () {
            OB.CUSTSDT.Utils.checkProdAndProdCatForDocumentType(retDocTypeSK, prodCatArr, prodArr, prod, isError, function () {
              count++;
              if (prod.get('isError')) {
                errors.push(prod.get('_identifier'));
                args.cancellation = true;
                isError = true;
              }
              if (args.selectedLines.length === count) {
                validateProdCatAndProdForVerifiedReturn(args, isError, errors);
              }
            });
          });
        }
      }, function () {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      });
    });

    validateProdCatAndProdForVerifiedReturn = function (args, isError, errors) {
      if (isError) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Preferred return document type does not allow article(s) ' + errors + ' for return');
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    };
  });

}());