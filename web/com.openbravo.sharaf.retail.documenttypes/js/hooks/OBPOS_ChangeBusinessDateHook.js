OB.UTIL.HookManager.registerHook('OBPOS_ChangeBusinessDateHook', function (args, callbacks) {
  OB.Dal.find(OB.Model.DocumentSequence, {}, function (data) {
    if (data && data.length > 0) {
      var docSeq = data.models[0];
      docSeq.set('documentSequence', 0);
      docSeq.set('quotationDocumentSequence', 0);
      docSeq.set('returnDocumentSequence', 0);
      OB.MobileApp.model.documentnoThreshold = 0;
      OB.MobileApp.model.quotationnoThreshold = 0;
      OB.MobileApp.model.returnnoThreshold = 0;
      OB.Dal.save(docSeq, function () {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }, function () {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      });
    }

  }, function () {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });


});