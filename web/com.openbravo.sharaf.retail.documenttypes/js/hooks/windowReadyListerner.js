/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, moment */

(function () {

  OB.MobileApp.model.on('window:ready', function () {
    if (OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('terminal').originalDocNoPrefix)) {
      OB.MobileApp.model.get('terminal').originalDocNoPrefix = OB.MobileApp.model.get('terminal').docNoPrefix;
    }
    var businessdate = OB.UTIL.localStorage.getItem('businessdate') ? moment(OB.UTIL.localStorage.getItem('businessdate')).format('DDMMYY') : moment().format('DDMMYY');
    OB.CUSTSDT.Utils.setDefaultDocumentPrefix(businessdate);
  });

  OB.Model.Order.prototype.includeDocNoSeperator = false;

}());