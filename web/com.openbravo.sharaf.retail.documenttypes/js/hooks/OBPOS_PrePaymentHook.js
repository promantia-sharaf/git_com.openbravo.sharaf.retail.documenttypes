/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, moment */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function (args, callbacks) {
    var order = args.context.get('order'),
        dtKey = order.get('custsdtDocumenttypeSearchKey'),
        endCustomer = false;

    var checkEmpty = function (property) {
        var value = order.get(property);
        return OB.UTIL.isNullOrUndefined(order.get('custshaWebsiterefno')) || value.trim() === '';
        };

    var sharafDocArr = OB.MobileApp.model.get('sharafDocumentType');
    for (i = 0; i < sharafDocArr.length; i++) {
      if (sharafDocArr[i].searchKey === dtKey && sharafDocArr[i].shfptEndCustomer) {
        endCustomer = true;
      }
    }

    if (!order.get('isQuotation') && order.get('bp').id === OB.MobileApp.model.get('terminal').businessPartner) {
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSDT_NotAllowAnonymousSales'));
      return;
    }

    if (endCustomer) {
        if (checkEmpty('custshaWebsiterefno') || checkEmpty('custshaCustomerName') || checkEmpty('custshaCustomerPhone') || checkEmpty('custshaCustomerEmail') || checkEmpty('custshaCustomerAddress')) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSHA_ErrEmptyCustomerInformation', [OB.CUSTSDT.Utils.getDocumentTypeBySearchKey(dtKey).name]));
          return;
        }
    } else if (!order.get('generatedFromQuotation')) {
      if (!order.get('isVerifiedReturn')) {
        order.set('custshaCustomerName', '');
        order.set('custshaCustomerPhone', '');
        order.set('custshaCustomerEmail', '');
        order.set('custshaCustomerAddress', '');
      }
    }

    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());