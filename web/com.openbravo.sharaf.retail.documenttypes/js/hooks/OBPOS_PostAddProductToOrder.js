/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, moment */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PostAddProductToOrder', function (
  args, callbacks) {
    if (args.options && args.options.isVerifiedReturn) {
      if (args.options.originalOrder.custshaWebsiterefno !== "" && args.receipt.get('custshaWebsiterefno') === "") {
        args.receipt.set('custshaWebsiterefno', args.options.originalOrder.custshaWebsiterefno);
        args.receipt.set('custshaCustomerName', args.options.originalOrder.custshaCustomerName);
        args.receipt.set('custshaCustomerPhone', args.options.originalOrder.custshaCustomerPhone);
        args.receipt.set('custshaCustomerEmail', args.options.originalOrder.custshaCustomerEmail);
        args.receipt.set('custshaCustomerAddress', args.options.originalOrder.custshaCustomerAddress);
        args.receipt.set('description', args.options.originalOrder.description);
      }
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());