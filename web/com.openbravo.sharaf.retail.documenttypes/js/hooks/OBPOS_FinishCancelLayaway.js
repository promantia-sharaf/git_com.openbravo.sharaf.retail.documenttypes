/*
 ************************************************************************************
 * Copyright (C) 2018-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_FinishCancelLayaway', function (args, callbacks) {
    var docType = _.find(OB.MobileApp.model.get('sharafDocumentType'), function (dt) {
      return dt.searchKey === 'AR';
    });
    if (docType) {
      var nextDocumentno = OB.MobileApp.model.getNextDocumentno(),
          documentNo = nextDocumentno.documentNo;
      documentNo = docType.prefix + documentNo.substring(2);
      OB.MobileApp.model.documentnoThreshold = nextDocumentno.documentnoSuffix;
      args.receipt.set('documentNo', documentNo, {
        silent: true
      });
      args.receipt.set('negativeDocNo', documentNo, {
        silent: true
      });
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());