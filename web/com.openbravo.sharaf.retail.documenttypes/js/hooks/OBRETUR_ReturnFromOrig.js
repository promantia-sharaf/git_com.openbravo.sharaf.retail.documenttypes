/**
 * Promantia Development
 */

OB.UTIL.HookManager.registerHook('OBRETUR_ReturnFromOrig', function (args, callbacks) {
  if (!OB.UTIL.isNullOrUndefined(args.context.model.get('order').get('isVerifiedReturn'))) {

    var OldDocType;
    try {

      if (!OB.UTIL.isNullOrUndefined(args.order.custsdtDocumenttype)) {
        var OldDocType = OB.COSRD.getSearchKeyFromDocumentType(args.order.custsdtDocumenttype); // To fetch old orders doctype
      }

      if (!OB.UTIL.isNullOrUndefined(OldDocType.preferredReturnDocumentType)) {
        var prefDocTypeSearchKey = OB.COSRD.getSearchKeyFromDocumentTypeName(OldDocType.preferredReturnDocumentType); // To fetch preferred doctype if old order
        args.context.model.get('order').set('custsdtDocumenttype', prefDocTypeSearchKey.id);
        args.context.model.get('order').set('custsdtDocumenttypeSearchKey', prefDocTypeSearchKey.searchKey);

        //To change return doc number based on document type (change the prefix)
        var curDocNoWithoutPrefix = args.context.model.get('order').get('documentNo').substring(2, args.context.model.get('order').get('documentNo').length);
        curDocNoWithoutPrefix = prefDocTypeSearchKey.searchKey + curDocNoWithoutPrefix;
        args.context.model.get('order').set('documentNo', curDocNoWithoutPrefix);

        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    } catch (exc) {
      console.error("Exception in OBRETUR_ReturnFromOrig.js file " + exc);
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  } else {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  }
});

OB.COSRD = OB.COSRD || {};
OB.COSRD.getSearchKeyFromDocumentType = OB.COSRD.getSearchKeyFromDocumentType || {};
OB.COSRD.getSearchKeyFromDocumentTypeName = OB.COSRD.getSearchKeyFromDocumentTypeName || {}

OB.COSRD.getSearchKeyFromDocumentType = function (documentType) {
  var oldOrderDocType = _.find(OB.MobileApp.model.get('sharafDocumentType'), function (item) {
    return item.id === documentType;
  });
  return oldOrderDocType;
};

OB.COSRD.getSearchKeyFromDocumentTypeName = function (documentTypeName) {
  var docType = _.find(OB.MobileApp.model.get('sharafDocumentType'), function (item) {
    return item.name === documentTypeName;
  });
  return docType;
};


//extending returnline function in pointofsale.js
//To change the document number and document type during blind returns
OB.OBPOSPointOfSale.UI.PointOfSale.extend({

  returnLine: function (inSender, inEvent) {

    var me = this;
    for (var i = 0; i < OB.MobileApp.model.receipt.get('lines').models.length; i++) {
      var line = OB.MobileApp.model.receipt.get('lines').models[i];
      if (line.get('product').get('returnable') && !OB.UTIL.isNullOrUndefined(line.get('product').get('digiProductType'))) {
        OB.UTIL.showError(OB.I18N.getLabel('CUSTSDT_digitalProductNotAllowed', [line.get('product').get('_identifier')]));
        return;
      }
    }

    var retDocumentType = OB.COSRD.getSearchKeyFromDocumentTypeName(OB.COSRD.getSearchKeyFromDocumentType(inSender.owner.model.attributes.order.attributes.custsdtDocumenttype).preferredReturnDocumentType).id,
        retDocTypeSK = OB.COSRD.getSearchKeyFromDocumentTypeName(OB.COSRD.getSearchKeyFromDocumentType(inSender.owner.model.attributes.order.attributes.custsdtDocumenttype).preferredReturnDocumentType).searchKey,
        prodArr = [],
        prodCatArr = [],
        count = 0,
        errors = [],
        isError = false;

    _.each(this.model.attributes.order.attributes.lines.models, function (receiptLines) {
      var prod = receiptLines.get('product');
      OB.CUSTSDT.Utils.fetchProdAndProdCatForDocumentType(retDocTypeSK, retDocumentType, prodCatArr, prodArr, prod, function () {
        OB.CUSTSDT.Utils.checkProdAndProdCatForDocumentType(retDocTypeSK, prodCatArr, prodArr, prod, isError, function () {
          count++;
          if (prod.get('isError')) {
            errors.push(prod.get('_identifier'));
            isError = true;
          }
          if (OB.MobileApp.model.receipt.get('lines').length === count) {
            if (isError) {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Article(s) ' + errors + ' not allowed in ' + retDocTypeSK + ' document type', [{
                label: OB.I18N.getLabel('OBMOBC_LblOk'),
                isConfirmButton: true,
                action: function () {
                  return;
                }
              }]);
            } else {
              if (inEvent.line.get('qty') > 0) {
                me.model.get('order').set('isPaid', true);
                var manualSetIsPaid = true;
                if (!OB.UTIL.isNullOrUndefined(OB.COSRD.getSearchKeyFromDocumentTypeName(OB.COSRD.getSearchKeyFromDocumentType(inSender.owner.model.attributes.order.attributes.custsdtDocumenttype).preferredReturnDocumentType)) && !OB.UTIL.isNullOrUndefined(OB.COSRD.getSearchKeyFromDocumentTypeName(OB.COSRD.getSearchKeyFromDocumentType(inSender.owner.model.attributes.order.attributes.custsdtDocumenttype).preferredReturnDocumentType).id)) {
                  var retDocType = OB.COSRD.getSearchKeyFromDocumentTypeName(OB.COSRD.getSearchKeyFromDocumentType(inSender.owner.model.attributes.order.attributes.custsdtDocumenttype).preferredReturnDocumentType).id;
                  var retDocTypeSearchKey = OB.COSRD.getSearchKeyFromDocumentTypeName(OB.COSRD.getSearchKeyFromDocumentType(inSender.owner.model.attributes.order.attributes.custsdtDocumenttype).preferredReturnDocumentType).searchKey;
                  var linkedSalesDocType = inSender.owner.model.attributes.order.attributes.custsdtDocumenttype;
                  var linkedSalesDocTypeSK = inSender.owner.model.attributes.order.attributes.custsdtDocumenttypeSearchKey;
                  me.model.get('order').set('linkedSalesDocType', linkedSalesDocType);
                  me.model.get('order').set('linkedSalesDocTypeSK', linkedSalesDocTypeSK);
                }
              } else {
                me.model.get('order').set('isPaid', true);
                var currRetDocType = me.model.get('order').get('custsdtDocumenttype');
                var returnDocTypeName = null;
                var sharafDocTypes = OB.MobileApp.model.get('sharafDocumentType');
                for (i = 0; i < sharafDocTypes.length; i++) {
                  var sharafDocType = sharafDocTypes[i].id;
                  if (currRetDocType === sharafDocType) {
                    returnDocTypeName = sharafDocTypes[i].name;
                    break;
                  }
                }

                var linkedSalesDocType, linkedSalesDocTypeSK;
                if (!OB.UTIL.isNullOrUndefined(me.model.get('order').get('linkedSalesDocType'))) {
                  linkedSalesDocType = me.model.get('order').get('linkedSalesDocType');
                } else {
                  linkedSalesDocType = inSender.owner.model.attributes.order.attributes.custsdtDocumenttype;
                }

                if (!OB.UTIL.isNullOrUndefined(me.model.get('order').get('linkedSalesDocTypeSK'))) {
                  linkedSalesDocTypeSK = me.model.get('order').get('linkedSalesDocTypeSK');
                } else {
                  linkedSalesDocTypeSK = inSender.owner.model.attributes.order.attributes.custsdtDocumenttypeSearchKey;
                }
              }

              var productStatus = OB.UTIL.ProductStatusUtils.getProductStatus(inEvent.line.get('product'));
              if (me.model.get('order').get('isEditable') === false) {
                me.doShowPopup({
                  popup: 'modalNotEditableOrder'
                });
                return true;
              }
              if (me.model.get('order').get('replacedorder') && inEvent.line.get('remainingQuantity')) {
                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('OBPOS_CancelReplaceReturnLines'));
                return;
              }
              if (OB.DEC.compare(inEvent.line.get('qty')) === -1 && productStatus && productStatus.restrictsaleoutofstock && OB.MobileApp.model.hasPermission('OBPOS_CheckStockForNotSaleWithoutStock', true)) {
                var qtyAdded = -inEvent.line.get('qty') - inEvent.line.get('qty');
                me.model.get('order').getStoreStock(inEvent.line.get('product'), qtyAdded, inEvent, null, function (hasStock) {
                  if (hasStock) {
                    me.model.get('order').returnLine(inEvent.line);
                  }
                });
              } else {
                me.model.get('order').returnLine(inEvent.line);
              }

              if (!OB.UTIL.isNullOrUndefined(retDocType)) {
                var docNo = retDocTypeSearchKey + me.model.get('order').get('documentNo').substring(2, me.model.get('order').get('documentNo').length);
                me.model.get('order').set('custsdtDocumenttype', retDocType); //doc type change
                me.model.get('order').set('custsdtDocumenttypeSearchKey', retDocTypeSearchKey);
                me.model.get('order').set('documentNo', docNo); //doc no change
                me.model.get('order').set('isPaid', false);
              } else if (!OB.UTIL.isNullOrUndefined(currRetDocType)) {
                var salesDocNo = linkedSalesDocTypeSK + me.model.get('order').get('documentNo').substring(2, me.model.get('order').get('documentNo').length);
                me.model.get('order').set('custsdtDocumenttype', linkedSalesDocType); // assigning sales document type
                me.model.get('order').set('custsdtDocumenttypeSearchKey', linkedSalesDocTypeSK);
                me.model.get('order').set('documentNo', salesDocNo); // assigning original Doc no
                me.model.get('order').set('isPaid', false);
              }

              //Fetch allowed delivery types for Blind returns
              var documentTypeId = me.model.get('order').get('custsdtDocumenttype');

              var query = "SELECT a.* from alloweddeltypes a where a.custsdt_documenttype_id ='" + documentTypeId + "'and a.active = 'true' order by a.priority asc";
              OB.Dal.queryUsingCache(OB.Model.AllowedDeliveryTypes, query, [], function (del) {
                if (del.length === 0) {
                  finalCallback();
                } else {
                  var deliveryArr = [];
                  for (var i = 0; i < del.models.length; i++) {
                    deliveryArr.push(del.models[i].attributes.custdelDeliverycondition);
                  }

                  if (deliveryArr.length > 0 && deliveryArr.includes(inEvent.line.get('cUSTDELDeliveryCondition'))) {
                    inEvent.line.set('cUSTDELDeliveryCondition', inEvent.line.get('cUSTDELDeliveryCondition'));
                  } else {
                    if (deliveryArr.length > 0) {
                      inEvent.line.set('cUSTDELDeliveryCondition', deliveryArr[0]);
                    }
                  }
                  localStorage.setItem('allowedDelivery', JSON.stringify(deliveryArr));

                }
              }, function () {
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
              });

              if (!OB.UTIL.isNullOrUndefined(manualSetIsPaid)) {
                me.model.get('order').set('isPaid', false);
              }
            }
          }
        });
      });
    });
  }
});