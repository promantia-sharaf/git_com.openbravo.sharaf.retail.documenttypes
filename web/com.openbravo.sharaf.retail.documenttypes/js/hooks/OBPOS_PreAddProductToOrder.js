/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, moment */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PreAddProductToOrder', function (args, callbacks) {
    if (args.cancelOperation) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    }

    if (args.receipt.get('custsdtDocumenttypeSearchKey') === 'BS') {
      var clonedProduct = new OB.Model.Product();
      OB.UTIL.clone(args.productToAdd, clonedProduct);
      clonedProduct.set('groupProduct', true);
      args.productToAdd = clonedProduct;
    }

    var documentSK = args.receipt.get('custsdtDocumenttypeSearchKey'),
        documentTypeId = args.receipt.get('custsdtDocumenttype'),
        prod = args.productToAdd,
        prodArr = [],
        prodCatArr = [],
        isError = false;


    OB.CUSTSDT.Utils.fetchProdAndProdCatForDocumentType(documentSK, documentTypeId, prodCatArr, prodArr, prod, function () {
      OB.CUSTSDT.Utils.checkProdAndProdCatForDocumentType(documentSK, prodCatArr, prodArr, prod, isError, function () {
        if (prod.get('isError')) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Article ' + prod.get('_identifier') + ' is not allowed in ' + documentSK + ' document type', [{
            label: OB.I18N.getLabel('OBMOBC_LblOk'),
            isConfirmButton: true,
            action: function () {
              return;
            }
          }]);
        } else {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
      });
    });
  });

}());