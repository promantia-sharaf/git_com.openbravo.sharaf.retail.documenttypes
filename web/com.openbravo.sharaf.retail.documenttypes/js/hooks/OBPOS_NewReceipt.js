/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _, Backbone */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_NewReceipt', function (args, callbacks) {
    // Set Sharaf Document Type
    args.newOrder.on('change:documentNo', OB.CUSTSDT.Utils.eventChangeDocumentNo);
    if (!OB.MobileApp.model.get('hasPaymentsForCashup')) {
      args.newOrder.set('isQuotation', true);
      args.newOrder.set('generateInvoice', false);
      args.newOrder.set('args.orderType', 0);
      args.newOrder.set('documentType', OB.MobileApp.model.get('terminal').terminalType.documentTypeForQuotations);
      var nextQuotationno = OB.MobileApp.model.getNextQuotationno();
      args.newOrder.set('quotationnoPrefix', OB.MobileApp.model.get('terminal').quotationDocNoPrefix);
      args.newOrder.set('quotationnoSuffix', nextQuotationno.quotationnoSuffix);
      args.newOrder.set('documentNo', nextQuotationno.documentNo);
    } else {
      var docType = OB.CUSTSDT.Utils.getDocumentType('ticketSequence', 'defaultTicket');
      OB.CUSTSDT.Utils.changeDocumentType(args.newOrder, new Backbone.Model(docType));
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());