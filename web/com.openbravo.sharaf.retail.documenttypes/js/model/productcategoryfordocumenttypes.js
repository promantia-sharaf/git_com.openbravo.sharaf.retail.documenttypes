/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global Backbone, _ */

(function () {
  var ProductCategoryForDocumentTypes = OB.Data.ExtensibleModel.extend({
    modelName: 'ProductCategoryForDocumentTypes',
    tableName: 'productcategoryfordocumenttypes',
    entityName: 'ProductCategoryForDocumentTypes',
    source: 'com.openbravo.sharaf.retail.documenttypes.service.ProductCategoryForDocumentTypes',
    dataLimit: OB.Dal.DATALIMIT
  });

  ProductCategoryForDocumentTypes.addProperties([{
    name: 'id',
    column: 'custsdt_product_cat_id',
    primaryKey: true,
    type: 'TEXT'
  }, {
    name: 'custsdtDocumenttype',
    column: 'custsdt_documenttype_id',
    type: 'TEXT'
  }, {
    name: 'productCategory',
    column: 'm_product_category_id',
    type: 'TEXT'
  }, {
    name: 'active',
    column: 'active',
    type: 'TEXT'
  }]);

  OB.Data.Registry.registerModel(ProductCategoryForDocumentTypes);
  OB.OBPOSPointOfSale.Model.PointOfSale.prototype.models.push(ProductCategoryForDocumentTypes);
})();