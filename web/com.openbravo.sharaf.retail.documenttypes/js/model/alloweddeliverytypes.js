/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global Backbone, _ */

(function () {
  var AllowedDeliveryTypes = OB.Data.ExtensibleModel.extend({
    modelName: 'AllowedDeliveryTypes',
    tableName: 'alloweddeltypes',
    entityName: 'AllowedDeliveryTypes',
    source: 'com.openbravo.sharaf.retail.documenttypes.service.AllowedDeliveryTypes',
    dataLimit: OB.Dal.DATALIMIT
  });

  AllowedDeliveryTypes.addProperties([{
    name: 'id',
    column: 'custsdt_alloweddeltypes_id',
    primaryKey: true,
    type: 'TEXT'
  }, {
    name: 'custdelDeliverycondition',
    column: 'custdelDeliverycondition',
    type: 'TEXT'
  }, {
    name: 'custsdtDocumenttype',
    column: 'custsdt_documenttype_id',
    type: 'TEXT'
  }, {
    name: 'priority',
    column: 'priority',
    type: 'NUMERIC'
  }, {
    name: 'active',
    column: 'active',
    type: 'TEXT'
  }]);

  AllowedDeliveryTypes.addIndex([{
    name: 'priority_index',
    columns: [{
      name: 'priority',
      sort: 'asc'
    }]
  }]);

  OB.Data.Registry.registerModel(AllowedDeliveryTypes);
  OB.OBPOSPointOfSale.Model.PointOfSale.prototype.models.push(AllowedDeliveryTypes);
})();