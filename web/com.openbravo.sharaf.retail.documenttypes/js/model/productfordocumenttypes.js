/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global Backbone, _ */

(function () {
  var ProductForDocumentTypes = OB.Data.ExtensibleModel.extend({
    modelName: 'ProductForDocumentTypes',
    tableName: 'productfordocumenttypes',
    entityName: 'ProductForDocumentTypes',
    source: 'com.openbravo.sharaf.retail.documenttypes.service.ProductForDocumentTypes',
    dataLimit: OB.Dal.DATALIMIT
  });

  ProductForDocumentTypes.addProperties([{
    name: 'id',
    column: 'custsdt_product_id',
    primaryKey: true,
    type: 'TEXT'
  }, {
    name: 'custsdtDocumenttype',
    column: 'custsdt_documenttype_id',
    type: 'TEXT'
  }, {
    name: 'product',
    column: 'm_product_id',
    type: 'TEXT'
  }, {
    name: 'active',
    column: 'active',
    type: 'TEXT'
  }]);

  OB.Data.Registry.registerModel(ProductForDocumentTypes);
  OB.OBPOSPointOfSale.Model.PointOfSale.prototype.models.push(ProductForDocumentTypes);
})();